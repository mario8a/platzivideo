import axios from 'axios';

// Add favorite
export const setFavorite = payload => ({
   type: 'SET_FAVORITE',
   payload,
});

//Delete favorite
export const deleteFavorite = payload => ({
   type: 'DELETE_FAVORITE',
   payload
});

//Login
export const loginRequest = payload => ({
   type: 'LOGIN_REQUEST',
   payload
});

//Cerrar seion
export const logoutRequest = payload => ({
   type: 'LOGOUT_REQUEST',
   payload
});

//register
export const registerRequest = payload => ({
   type: 'REGISTER_REQUEST',
   payload
})

// Playing
export const getVideosSource = payload => ({
   type: 'GET_VIDEO_SOURCE',
   payload
});

//Search
export const getVideoSearch = payload => ({
   type: 'GET_VIDEO_SEARCH',
   payload,
});

export const setError = payload => ({
   type: 'SET_ERROR',
   payload
})

export const registerUser = (payload, redirectUrl) => async (dispatch) => {
   try {
     const { data } = await axios.post('/auth/sign-up', payload);
     dispatch(registerRequest(data));
     window.location.href = redirectUrl;
   } catch (error) {
     console.log(error);
   }
 };
 //localhost:3000/login

 export const loginUser = ({ email, password }, redirectUrl) => {
   return (dispatch) => {
     axios({
       url: '/auth/sign-in',
       method: 'post',
       auth: {
         username: email,
         password
       },
     })
       .then(({ data }) => {
         document.cookie = `email=${data.user.email}`;
         document.cookie = `name=${data.user.name}`;
         document.cookie = `id=${data.user.id}`;
         dispatch(loginRequest(data.user));
       })
       .then(() => {
         window.location.href = redirectUrl;
       })
       .catch(err => dispatch(setError(err)));
   }
 };
 
 export { setFavorite as default }