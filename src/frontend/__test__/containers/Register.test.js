import React from 'react';
import { mount } from 'enzyme';
import Register from '../../containers/Register';
import ProviderMock from '../../__mocks__/ProviderMock';
import { expect, jest } from '@jest/globals';

describe('<Register />', () => {
   test('Register form ', () => {
      const preventDefault = jest.fn();
      const register = mount(
         <ProviderMock>
            <Register />
         </ProviderMock>
      );

      //Simulando que le damos submit
      register.find('form').simulate('submit', {preventDefault});
      expect(preventDefault).toHaveBeenCalledTimes(1); //Simula que se llama una vez al submit, si hace dos falla
      register.unmount();
   });
   
});
