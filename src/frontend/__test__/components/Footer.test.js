import React from 'react';
import { mount, shallow } from 'enzyme';
import { create } from 'react-test-renderer';
import Footer from '../../components/Footer';

describe('<Footer />', () => {
   
   const footer = mount(<Footer/>);

   test('Render Footer component', () => {
      expect(footer.length).toEqual(1);
   });
   
   test('Footer haves 3 anchors', () => {
      expect(footer.find('a')).toHaveLength(3);
   });

   test('Footer snapshot', () => {
      const footer = create(<Footer />);
      expect(footer.toJSON()).toMatchSnapshot();
   })
   
});


// Los snapshots nos van a permitir que nuestra ui no cambie bruscamente. 
// Que no se mande a producción con doble navbar, doble logo etc. 
// Siempre avisando cuando hay algun cambio y fallando la prueba.
// Si necesitomos actualizar nuestro snapshot inicial debemos correr el comando.

// Si cambia el componente el snapshot debe cambiar, por lo que debemos ejecutar
// jest --updateSnapshot ´para actualizarlo