import { expect } from '@jest/globals';
import gravatar from '../../utils/gravatar';

test('Gravatar function test', () => {
   const email = 'mario8ato@hotmail.com';
   const graatarUrl = 'https://gravatar.com/avatar/61166fc1b4ba4997cb7d8f6f0f998d66'
   expect(graatarUrl).toEqual(gravatar(email))
});
