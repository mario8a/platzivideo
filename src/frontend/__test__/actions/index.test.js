import { expect } from '@jest/globals';
import { setFavorite, loginRequest } from '../../actions/index';
import movieMock from '../../__mocks__/movieMock';

describe('Actions', () => {
   test('set favorite', () => {
      const payload = movieMock;
      const expectedActions = {
         type: 'SET_FAVORITE',
         payload
      }

      expect(setFavorite(payload)).toEqual(expectedActions);
   });

   test('Login', () => {
      const payload = {
         email: 'test@test.com',
         password: 'password'
      }
      const extectedAction = {
         type: 'LOGIN_REQUEST',
         payload
      }
      expect(loginRequest(payload)).toEqual(extectedAction)
   })
   
   
});
