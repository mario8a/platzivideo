import React from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import '../assets/styles/components/Search.scss';
import { getVideoSearch } from '../actions';

const Search = ({isHome, getVideoSearch }) => {

   const inputStyle = classNames('input', {
      isHome
   })

   const handleInput = (event) => {
      getVideoSearch(event.target.value);       
  }

   return (
      <section className="main">
         <h2 className="main__title">¿Qué quieres ver hoy?</h2>
         <input
            onKeyUp={handleInput}
            type="text" 
            className={inputStyle} 
            placeholder="Buscar..." />
      </section>
   )
};

const mapStateToProps = state => {
   return {
       searchResult: state.searchResult,
   }
}

const mapDispatchToProps = {
   getVideoSearch,
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);