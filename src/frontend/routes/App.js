import React from 'react';
import { BrowserRouter, Switch ,Route } from 'react-router-dom';
import Home from '../containers/Home';
import Layout from '../containers/Layout';
import Login from '../containers/Login';
import NotFound from '../containers/NotFound';
import Player from '../containers/Player';
import Register from '../containers/Register';

const App = ({isLogged}) => (
   <BrowserRouter>
      <Layout>
         <Switch>
            <Route exact path="/" component={ isLogged ? Home: Login}/>
            <Route exact path="/login" component={Login} />
            <Route exact  path="/register" component={Register} />
            <Route exact path="/player/:id" component={isLogged ? Player: Login} />
            <Route  component={NotFound} />
         </Switch>
      </Layout>
   </BrowserRouter>
);

export default App;