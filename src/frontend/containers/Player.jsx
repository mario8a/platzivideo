import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getVideosSource } from '../actions';
import '../assets/styles/components/Player.scss';
import NotFound from './NotFound';

const Player = (props) => {
   //Obteniendo id de los params
   const { id } = props.match.params;

   //validar si hay un video que se esta ejecutando o si existe
   const hasPlaying = Object.keys(props.playing).length > 0;

   useEffect(() => {
      props.getVideosSource(id);
   }, [])

   return hasPlaying ? (
      <div className="Player">
         <video controls autoPlay>
            <source src={props.playing.source} type="videi/mp4" />
         </video>
         <div className="Player-back">
            <button type="button" onClick={() => props.history.goBack()}>
               Regresar
            </button>
         </div>
      </div>
   ): <NotFound /> ;
};

const mapStateToProps = state => {
   return {
      playing: state.playing,
   };
};

const mapDispatchToProps = {
   getVideosSource,
}

export default connect(mapStateToProps, mapDispatchToProps)(Player);