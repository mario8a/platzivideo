import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import '../assets/styles/App.scss';
import Carousel from '../components/Carousel';
import CarouselItem from '../components/CarouselItem';
import Categories from '../components/Categories';
import Header from '../components/Header';
import Search from '../components/Search';
import useInitialState from '../hooks/useInitialState';


const Home = ({myList, trends, originals, searchResult}) => {

   return (
      <>
         <Header />
         <Search isHome />

         {Object.keys(searchResult).length > 0 && 
            (
               <Categories title="Resultados de la busqueda...">
                  <Carousel>
                      {searchResult.map(item =>
                          <CarouselItem 
                          key={item.id} 
                          {...item}
                          />
                      )}
                  </Carousel>
               </Categories>
            )                        
         }

         { myList.length > 0 && 
            (<Categories title="Mi lista">
               <Carousel>
               {myList.map(item => 
                  <CarouselItem 
                     key={item.id} 
                     {...item}
                     isList
                  />
               )}
               </Carousel>
            </Categories>)
         }


         <Categories title="Tendencias">
            <Carousel>
               {trends.map(item => 
                  <CarouselItem key={item.id} {...item} />
               )}
            </Carousel>
         </Categories>

         <Categories title="Originales">
            <Carousel>
               {originals.map(item => 
                  <CarouselItem key={item.id} {...item} />
               )}
            </Carousel>
         </Categories>
      </>
   );
};
//Va trear los props del estado
const mapStateToProps = state => {
   return {
      myList: state.myList,
      trends: state.trends,
      originals: state.originals,
      searchResult: state.searchResult
   };
};

// export default Home;
// export default connect(props, actions)(Home);
export default connect(mapStateToProps, null)(Home);