import express from 'express';
import dotenv from 'dotenv';
import  webpack from 'webpack';
import React from 'react';
import helmet from 'helmet';
import {renderToString} from 'react-dom/server';
import { Provider } from 'react-redux';
import { createStore, compose } from 'redux';
import { renderRoutes } from 'react-router-config';
import { StaticRouter } from 'react-router-dom';
import serverRoutes from '../frontend/routes/serverRoutes';
import reducer from '../frontend/reducers'; 
// import initialState from '../frontend/initialState';
import getManifest from './getManifest';

import cookieParser from 'cookie-parser';
import boom from '@hapi/boom';
import passport from 'passport';
import axios from 'axios';


dotenv.config();

const {ENV, PORT} = process.env;
const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

require('./utils/auth/strategies/basic');

// if(ENV === 'development') {
   //Da error ValidationError
//    const webpackConfig = require('../../webpack.config');
//    const webpackDevMiddleware = require('webpack-dev-middleware');
//    const webpackHotMiddleware = require('webpack-hot-middleware');
   
//    const compiler = webpack(webpackConfig);
//    const serverConfig = { port: PORT, hot: true };

//    app.use(webpackDevMiddleware(compiler, serverConfig));
//    app.use(webpackHotMiddleware(compiler));
// }

if (ENV === 'development') {
   console.log('Development config');
   const webpackConfig = require('../../webpack.config');
   const webpackDevMiddleware = require('webpack-dev-middleware');
   const webpackHotMiddleware = require('webpack-hot-middleware');
   const compiler = webpack(webpackConfig);
   const { publicPath } = webpackConfig.output;
   const serverConfig = { serverSideRender: true, publicPath };
 
   app.use(webpackDevMiddleware(compiler, serverConfig));
   app.use(webpackHotMiddleware(compiler));
} else {
   app.use((req, res, next) => {
      if (!req.hashManifest) req.hashManifest = getManifest();
      next();
    });
   app.use(express.static(`${__dirname}/public`));
   app.use(helmet());
   app.use(helmet.permittedCrossDomainPolicies());
   app.disable('x-powered-by');
}

const setResponse = (html, preloadedState, manifest) => {
   const mainBuild = manifest ? manifest['main.js'] : 'assets/app.js';
   const vendorBuild = manifest ? manifest['vendors.js'] : 'assets/vendor.js';
   const mainStyles = manifest ? manifest['vendors.css'] : 'assets/app.css'; //al parecer splitChunks genera un vendor para los css y lo mapea en el manifest con otro nombre

   return (`
   <!DOCTYPE html>
   <html lang="en">
      <head>
         <meta charset="UTF-8">
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="${mainStyles}" type="text/css">
         <title>Platzi Video</title>
      </head>
      <body>
         <div id="app"> ${html} </div>
         <script>
         window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
         </script>
         <script src="${mainBuild}" type="text/javascript"></script>
         <script src="${vendorBuild}" type="text/javascript"></script>
      </body>
   </html>
   `);
}

const renderApp = async (req, res) => {

   let initialState;
   const { token ,email , name , id } = req.cookies;

   try {
      let movieList = await axios({
         url: `${process.env.API_URL}/api/movies`,
         headers: { Authorization : `Bearer ${token}`},
         method: 'get'
      });
      movieList = movieList.data.data;
      initialState = {
         user: {
            id, email, name
         },
         myList: [],
         searchResult: [],
         trends: movieList.filter(movie => movie.contentRating === 'PG' && movie._id),
         originals: movieList.filter(movie => movie.contentRating === 'G' && movie._id)
      }
   } catch(err) {
      initialState = {
         user: {},
         myList: [],
         searchResult: [],
         trends: [],
         originals: []
      }
   }

   const store = createStore(reducer, initialState);
   const preloadedState = store.getState();
   const isLogged = (initialState.user.id);
   const html = renderToString(
      <Provider store={store}>
         <StaticRouter location={req.url} context={{}}>
            {renderRoutes(serverRoutes(isLogged))}
         </StaticRouter>
      </Provider>,
   );
   // res.set("Content-Security-Policy", "default-src 'self'; img-src 'self' http://dummyimage.com; script-src 'self' 'sha256-T4gMAi7VL1GyW3+77Ol9xE1S/cFPb4d64iaMXBdt/vY='; style-src-elem 'self' https://fonts.googleapis.com; font-src https://fonts.gstatic.com");
   // res.set(
	// 	"Content-Security-Policy",
	// 	"script-src 'self' 'sha256-Xx/mBO5zOQb/jAyWEWppl3dp/QW2st+qLNseeOmUzoU='",
	// );
   // res.set("Content-Security-Policy", "img-src 'self' http://dummyimage.com")
   res.set("Content-Security-Policy", "default-src *; style-src 'self' http://* 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'")
   res.send(setResponse(html, preloadedState, req.hashManifest));
}

app.post("/auth/sign-in", async function(req, res, next) {
  passport.authenticate("basic", function(error, data) {
   try {
     if (error || !data) {
       next(boom.unauthorized());
     }

     req.login(data, { session: false }, async function(err) {
       if (err) {
         next(err);
       }
       const { token, ...user } = data;
        // Si el atributo rememberMe es verdadero la expiración será en 30 dias
       // de lo contrario la expiración será en 2 horas
       res.cookie("token", token, {
         httpOnly: !(ENV === 'development'),
         secure: !(ENV === 'development'),
       });

       res.status(200).json(user);
     });
   } catch (err) {
     next(err);
   }
 })(req, res, next);
});

app.post("/auth/sign-up", async function (req, res, next) {
   const { body: user } = req;
 
   try {
     const userData = await axios({
       url: `${process.env.API_URL}/api/auth/sign-up`,
       method: "post",
       data: {
         'email': user.email,
         'name': user.name,
         'password': user.password
       }
     });
     res.status(201).json({
       name: req.body.name,
       email: req.body.email,
       id: userData.data.id
     });
   } catch (error) {
     next(error);
   }
 });

app.get('*', renderApp);

app.listen(PORT, (err) => {
   if(err) console.log(err);
   else console.log(`Server running on port ${PORT}`)
});